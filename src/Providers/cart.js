import { createContext, useState } from "react"

export const CartContext = createContext([])

export const CartProvider = ({ children }) => {
	const [cart, setCart] = useState([])

	const addToCart = (item) => setCart([...cart, item])

	const removeFromCart = (item) => {
		const new_cart = cart.filter((ci) => ci.name !== item.name)

		setCart(new_cart)
	}

	return (
		<CartContext.Provider value={{ cart, addToCart, removeFromCart }}>
			{children}
		</CartContext.Provider>
	)
}
