import "./App.css"
import ProductList from "./components/ProductList"
import { CartProvider } from "./Providers/cart"
import { CatalogueProvider } from "./Providers/catalogue"

function App() {
	return (
		<div className="App">
			<CatalogueProvider>
				<CartProvider>
					<div>
						<h2>Catalogue</h2>
						<ProductList type="catalogue" />
					</div>
					<div>
						<h2>Cart</h2>
						<ProductList type="cart" />
					</div>
				</CartProvider>
			</CatalogueProvider>
		</div>
	)
}

export default App
