import { useContext } from "react"

import { CatalogueContext } from "../../Providers/catalogue"
import { CartContext } from "../../Providers/cart"

const Button = ({ type, item }) => {
	const { addToCart, removeFromCart } = useContext(CartContext)
	const { addToCatalogue, removeFromCatalogue } = useContext(CatalogueContext)

	const text = type === "catalogue" ? "Add to cart" : "Remove from Cart"

	const handleClick = () => {
		if (type === "catalogue") {
			removeFromCatalogue(item)
			addToCart(item)
		} else {
			removeFromCart(item)
			addToCatalogue(item)
		}
	}

	return <button onClick={handleClick}>{text}</button>
}

export default Button
