import { useContext } from "react"

import Button from "../Button"

import { CatalogueContext } from "../../Providers/catalogue"
import { CartContext } from "../../Providers/cart"

const ProductList = ({ type }) => {
	const { cart } = useContext(CartContext)
	const { catalogue } = useContext(CatalogueContext)

	if (type === "cart" && !cart.length) return <p>Empty</p>

	return (
		<ul>
			{type === "catalogue" &&
				catalogue.map((item, index) => (
					<li key={index}>
						{item.name} <Button type={type} item={item} />
					</li>
				))}

			{type === "cart" &&
				cart.map((item, index) => (
					<li key={index}>
						{item.name} <Button type={type} item={item} />
					</li>
				))}
		</ul>
	)
}

export default ProductList
